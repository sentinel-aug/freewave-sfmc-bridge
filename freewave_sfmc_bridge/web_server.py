import sys

from flask import Flask, render_template
from flask_socketio import SocketIO, emit

from . import Bridge


app = Flask(__name__)
socketio = SocketIO(app)


bridge = None


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('connect')
def test_connect():
    emit('after connect',  {'data': 'Lets dance'})


@socketio.on('start')
def start_bridge(message):
    global bridge
    if bridge:
        bridge.stop()
    vehicle_name = message['data']
    bridge = Bridge(vehicle_name, args.serial_port,
                    args.sfmc_host, args.sfmc_port,
                    password=args.password,
                    use_cd=not args.ignore_cd,
                    freewave_recorder=args.freewave_recorder,
                    sfmc_recorder=args.sfmc_recorder)


def main():
    import argparse

    parser = argparse.ArgumentParser(
        description='Bi-directional Freewave SFMC Bridge'
    )

    parser.add_argument('serial_port', help='serial port name')
    parser.add_argument('sfmc_host', help='SFMC hostname')
    parser.add_argument('--password', help='Bridge password', default=None)
    parser.add_argument('--sfmc-port', type=int,
                        help='SFMC port number', default=9045)
    parser.add_argument('-e', '--echo', action='store_true',
                        help='Echo everything to stdout',
                        default=False)
    parser.add_argument('--ignore-cd', action='store_true',
                        help='Ignore carrier detect',
                        default=False)
    parser.add_argument('--flask-host', help='Flask host',
                        default='0.0.0.0')
    parser.add_argument('--flask-port', help='Flask port',
                        type=int,
                        default=5000)

    global args
    args = parser.parse_args()

    def print_to_stdout(s):
        sys.stdout.write(s)
        sys.stdout.flush()

    def sfmc_recorder(b):
        try:
            s = b.decode('utf-8')
            if args.echo:
                print_to_stdout(s)
            socketio.emit('from sfmc', {'data': s})
        except Exception:
            pass

    def freewave_recorder(b):
        try:
            s = b.decode('utf-8')
            if args.echo:
                print_to_stdout(s)
            socketio.emit('from glider', {'data': s})
        except Exception:
            pass

    args.sfmc_recorder = sfmc_recorder
    args.freewave_recorder = freewave_recorder

    socketio.run(app, host=args.flask_host, port=args.flask_port)

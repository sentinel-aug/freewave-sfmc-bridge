import logging
from select import select
import socket
import sys
from threading import Thread
import time

import serial

logger = logging.getLogger(__name__)


def bytes_has_binary(b):
    return any(map(lambda x: x not in b'\r\n' and not (32 <= x <= 126), b))


class Bridge:
    def __init__(self, vehicle_name, serial_port_path, sfmc_host, sfmc_port,
                 password=None,
                 use_cd=True,
                 freewave_recorder=None, sfmc_recorder=None,
                 ping_period=60):
        self.vehicle_name = vehicle_name
        self.serial_port_path = serial_port_path
        self.sfmc_host = sfmc_host
        self.sfmc_port = sfmc_port
        self.password = password
        self.use_cd = use_cd
        self.freewave_recorder = freewave_recorder or (lambda b: None)
        self.sfmc_recorder = sfmc_recorder or (lambda b: None)
        self.last_binary = 0

        self.ser = None
        self.sock = None
        self.last_connection_attempt = 0
        self.should_stop = False

        self.last_msg_sent_at = time.time()
        self.ping_period = ping_period

        self.thread = Thread(target=self.run, daemon=True)
        self.thread.start()

    def open_serial_port(self):
        path = self.serial_port_path
        ser = None
        try:
            ser = serial.Serial(path, 115200, timeout=0)
        except serial.SerialException as e:
            logger.error('Could not open serial port %s: %s', path, e)
        except Exception as e:
            logger.error('Unknown error opening serial port %s: %s', path, e)
        return ser

    def open_socket(self):
        host = self.sfmc_host
        port = self.sfmc_port
        logger.info('Opening connection to %s:%s...', host, port)
        sock = socket.socket()
        try:
            sock.connect((host, port))
            logger.info('Connected')
        except socket.error as e:
            logger.error('Unable to connect to %s:%s: %s', host, port, e)
            sock = None
        except Exception as e:
            logger.error('Unable to connect to %s:%s: %s', host, port, e)
            sock = None

        return sock

    def safe_socket_send(self, b):
        sock = self.sock
        self.last_msg_sent_at = time.time()
        try:
            if sock:
                sock.sendall(b)
        except socket.error as e:
            logger.warn('Unable to send message to SFMC: %s', e)
            self.sock = None
            sock.close()

    def send_herald(self):
        if self.password is not None:
            msg_1 = self.vehicle_name.encode('utf-8') \
                + b'\r\n'
            msg_2 = self.password.encode('utf-8') \
                + b'\r\n\r\n'
            self.safe_socket_send(msg_1)
            self.freewave_recorder(msg_1)
            self.safe_socket_send(msg_2)
            self.freewave_recorder(msg_2)

        msg_1 = b'\r\n# Connected with Freewave/SFMC Bridge\r\n'
        msg_2 = b'\r\nVehicle Name: ' \
            + self.vehicle_name.encode('utf-8') \
            + b'\r\n\r\n'

        self.safe_socket_send(msg_1)
        self.freewave_recorder(msg_1)
        self.safe_socket_send(msg_2)
        self.freewave_recorder(msg_2)

    def pump_once(self):
        ser = self.ser

        # Ensure the serial port is open. If we can't open it, there's no point
        # in continuing further.
        if ser is None:
            ser = self.open_serial_port()
            if ser is None:
                time.sleep(1)
                return
            else:
                self.ser = ser

        to_read = [ser]

        # Now, check if we have carrier detect. If we do, ensure the socket to
        # SFMC is open.
        sock = self.sock
        if self.use_cd:
            cd = ser.cd
        else:
            cd = True

        if cd and not sock:
            # Try not to spam SFMC with reconnection attempts.
            if time.time() - self.last_connection_attempt >= 5:
                # Open the socket
                sock = self.open_socket()
                self.last_connection_attempt = time.time()
                if sock:
                    self.sock = sock
                    self.send_herald()
                    # It's possible that send_herald caused the socket to be
                    # closed and set to None.
                    sock = self.sock
        elif not cd and sock:
            # Close the socket.
            sock.close()
            sock = None
            self.sock = None
            logger.info('Lost CD, socket closed')
        elif sock:
            # CD *and* socket
            to_read.append(sock)

        readable, _, _ = select(to_read, [], [], 0.1)

        if sock in readable:
            # There's data waiting for us from SFMC!
            data = sock.recv(1024)
            if not data:
                # Socket is actually closed!
                sock.close()
                sock = None
                self.sock = None
                logger.warn('SFMC closed the socket')
            else:
                if bytes_has_binary(data):
                    self.last_binary = time.time()

                if self.last_binary < (time.time() - 10):
                    # Slow down by putting a small sleep in between characters.
                    for i in range(len(data)):
                        ser.write(data[i:i+1])
                        time.sleep(0.1)
                else:
                    # There's binary data recently. Go full speed.
                    ser.write(data)
                self.sfmc_recorder(data)

        if ser in readable:
            # There's data waiting for us from the serial port!
            data = ser.read(256)
            if data and len(data) != 0:
                if sock:
                    self.safe_socket_send(data)
                self.freewave_recorder(data)

        current_time = time.time()
        if sock and (current_time - self.last_msg_sent_at) >= self.ping_period:
            self.safe_socket_send(b'\r\n# Ping '
                                  + str(current_time).encode('utf-8')
                                  + b'\r\n')

    def stop(self):
        self.should_stop = True
        self.thread.join()

    def run(self):
        while not self.should_stop:
            self.pump_once()
        if self.sock:
            self.sock.close()
        if self.ser:
            self.ser.close()


def main():
    import argparse

    parser = argparse.ArgumentParser(
        description='Bi-directional Freewave SFMC Bridge'
    )

    parser.add_argument('serial_port', help='serial port name')
    parser.add_argument('vehicle_name', help='vehicle name')
    parser.add_argument('sfmc_host', help='SFMC hostname')
    parser.add_argument('--sfmc-port', type=int,
                        help='SFMC port number', default=9045)
    parser.add_argument('--password', help='Bridge password', default=None)
    parser.add_argument('-e', '--echo', action='store_true',
                        help='Echo everything to stdout',
                        default=False)
    parser.add_argument('--ignore-cd', action='store_true',
                        help='Ignore carrier detect',
                        default=False)

    args = parser.parse_args()

    def print_to_stdout(b):
        try:
            sys.stdout.write(b.decode('utf-8'))
            sys.stdout.flush()
        except Exception:
            pass

    if args.echo:
        recorder = print_to_stdout
    else:
        recorder = None

    bridge = Bridge(args.vehicle_name, args.serial_port, args.sfmc_host,
                    args.sfmc_port,
                    args.password,
                    use_cd=not args.ignore_cd,
                    freewave_recorder=recorder,
                    sfmc_recorder=recorder)

    try:
        bridge.thread.join()
    except KeyboardInterrupt:
        pass

FROM python:3.9-alpine as builder

ENV VIRTUAL_ENV="/opt/venv"
RUN python3 -m venv "$VIRTUAL_ENV"
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

WORKDIR /app
COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY setup.py ./setup.py
COPY MANIFEST.in ./MANIFEST.in
COPY freewave_sfmc_bridge ./freewave_sfmc_bridge
RUN pip install ".[flask]"

FROM python:3.9-alpine

ENV VIRTUAL_ENV="/opt/venv"
COPY --from=builder $VIRTUAL_ENV $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

CMD ["freewave-sfmc-bridge"]

## Unreleased

## v0.2.0 - June 11, 2021

+ Rewrote code to be usable as a library.
+ Docker image uses a virtual environment.
+ Added a primitive web interface to watch data flowing over the bridge.
+ Release to project's Gitlab PyPI repo.

## v0.1.2 - August 24, 2020

### Fixed
+ Ignore errors when echoing to stdout. When transferring files, non-UTF-8
  sequences can be passed back and forth.

## v0.1.1 - August 4, 2020

### Fixed
+ CI builds now (hopefully) reuses more layers.
+ Fix writing to stdout.

## v0.1.0 - August 4, 2020

First release.

from setuptools import setup

required = [
    'pyserial'
]

extras = {
    'flask': [
        'flask',
        'flask-socketio'
    ]
}

setup(
    name="freewave-sfmc-bridge",
    version='0.3.7',
    description="Bi-directional Freewave/SFMC Bridge",
    packages=[
        "freewave_sfmc_bridge"
    ],
    include_package_data=True,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'freewave-sfmc-bridge=freewave_sfmc_bridge:main',
            'freewave-sfmc-bridge-web=freewave_sfmc_bridge.web_server:main[flask]'
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX"
        "Development Status :: 4 - Beta"
    ],
    install_requires=required,
    extras_require=extras,
)
